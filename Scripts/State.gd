extends Node
class_name State

var state_machine = null

func handle_input (_event: InputEvent) -> void:
	pass

func update (_delta: float) -> void:
	pass
	
func physics_update (_delta: float) -> void:
	pass

# Called by the state machine to initialize the state.
func enter (_msg:= {}) -> void:
	pass 

# Called before transitioning to another state. Use it to clean-up states.
func exit () -> void:
	pass 
