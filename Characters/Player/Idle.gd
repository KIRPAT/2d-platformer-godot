extends PlayerState

func enter(_msg:= {}) -> void:
	print("Idle")
	
	player.animation_player.play("Idle")
	#player.velocity = Vector2.ZERO
	
	
func physics_update(delta: float) -> void:
	#If platform breaks while standing on them.
	  
	#print(player.velocity.y)
	#print(player.is_on_floor())
	should_player_fall()
	handle_jump_input()
	handle_horizontal_moevement_input()

func should_player_fall():
	if not player.is_on_floor() and !player.is_on_wall():
		print("Switching to air state")
		state_machine.transition_to("Air")

func handle_jump_input():
	if Input.is_action_just_pressed("ui_accept"):
		state_machine.transition_to("Air", {do_jump = true})
		
func handle_horizontal_moevement_input():
	if Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right"):
		state_machine.transition_to("Walk")
