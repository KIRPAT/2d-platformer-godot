extends PlayerState

# Utility Functions #
func apply_friction():
	player.velocity.x = move_toward(player.velocity.x, 0, player.FRICTION)

func apply_gravity():
	if player.velocity.y < player.MAX_FELL_SPEED:
		player.velocity.y += player.GRAVITY
		if player.velocity.y > 0 and player.velocity.y < player.MAX_FELL_SPEED:
			player.velocity.y += player.FAST_FELL_ADDITIONAL_GRAVITY


func enter(_msg:= {}) -> void:
	print("Death")
	player.animation_player.play("Death")
	#TODO: "Restart" the scene from the "checkpoint".
	
func physics_update(delta: float) -> void:
	apply_gravity()
	apply_friction()
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)	

