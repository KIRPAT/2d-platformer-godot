extends PlayerState

# Utility Functions #
func apply_friction():
	player.velocity.x = move_toward(player.velocity.x, 0, player.FRICTION)

func apply_acceleration(amount):
	player.velocity.x = move_toward(player.velocity.x, player.HORIZONTAL_TOP_SPEED*sign(amount), player.HORIZONTAL_ACCELERATION)

func apply_gravity():
	if player.velocity.y < player.MAX_FELL_SPEED:
		player.velocity.y += player.GRAVITY
		if player.velocity.y > 0 and player.velocity.y < player.MAX_FELL_SPEED:
			player.velocity.y += player.FAST_FELL_ADDITIONAL_GRAVITY

# PlayerState functions
func enter(msg: = {}) -> void:
	if msg.has("do_jump"):
		player.animation_player.play("Jump - Ascend")
		jump()
	else:
		player.animation_player.play("Jump - Descend")
		print("Air")
	

func physics_update(delta: float) -> void:
	handle_horizontal_input()
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	handle_animation_state()
	apply_gravity()
	handle_direction_change()
	handle_dash_input()
	handle_if_landing()
	
# ACTIONS #
func jump() -> void:
	player.velocity.y = -player.JUMP_FORCE
	player.fast_fell = true

# HANDLERS #
func handle_horizontal_input() -> void:
	var input: float = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if input == 0:
		apply_friction()
	else:
		apply_acceleration(input)
		
func handle_dash_input() -> void:
	if Input.is_action_just_pressed("ui_lb"):
		state_machine.transition_to("Dash")

func handle_direction_change() -> void:
	var input: float = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if player.direction == player.directions.RIGHT and input < 0:
		player.direction = player.directions.LEFT
	elif player.direction == player.directions.LEFT and input > 0:
		player.direction = player.directions.RIGHT

func handle_if_landing() -> void:
	if player.is_on_floor():
		if is_equal_approx(player.velocity.x, 0.0):
			state_machine.transition_to("Idle")
		else: 
			state_machine.transition_to("Walk")

func handle_animation_state() -> void:
	if player.velocity.y < 0:
		player.animation_player.play("Jump - Ascend")
	if player.velocity.y > 5:
		player.animation_player.play("Jump - Descend")
	if player.velocity.y == 0:
		player.animation_player.play("Dash")
