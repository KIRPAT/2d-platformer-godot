extends KinematicBody2D
class_name Player
export (Resource) var MOVEMENT_DATA

var velocity = Vector2.ZERO

var sample_reset_position: Vector2 = Vector2(54,36)

var is_gravity_on = true
var fast_fell = false
var additional_jump_state: int
var current_direction = 1
var is_dash_on = false
var is_powerup_on = false

onready var defaultMovementData = load("res://Characters/DefaultPlayerMovementData.tres")  
onready var fastMovementData = load("res://Characters/FastPlayerMovementData.tres")
onready var ladderCheckRayCast = $LadderChack

func _init():
	z_index = 1

func _ready():
	MOVEMENT_DATA = defaultMovementData
	additional_jump_state = MOVEMENT_DATA.ADDITIONAL_JUMP_LIMIT

func _physics_process(delta):
	apply_gravity()
	handle_direction()
	handle_horizontal_input()
	reset_additional_jump_limit()
	handle_jump()
	handle_dash()
	reset_dash_state()
	handle_fall()
	handle_power_up()
	handle_ladder_movement()
	# print(position)
	
	var was_in_air = !is_on_floor()
	velocity = move_and_slide(velocity, Vector2.UP)
	var is_just_landed = was_in_air and is_on_floor()
	handle_animation_state(is_just_landed)
	#print(Performance.get_monitor(Performance.TIME_FPS))

func handle_power_up():
	if Input.is_action_just_pressed("ui_rb"):
		if is_powerup_on:
			is_powerup_on = false
			MOVEMENT_DATA = defaultMovementData
		else:
			is_powerup_on = true
			MOVEMENT_DATA = fastMovementData

func handle_ladder_movement():
	if ladderCheckRayCast.is_colliding():
		if ladderCheckRayCast.get_collider() is Ladder:
			if Input.is_action_pressed("ui_up"):
				if is_gravity_on == true: 
					is_gravity_on = false
				velocity.y = -MOVEMENT_DATA.LADDER_SPEED
				#print(MOVEMENT_DATA.LADDER_SPEED)
			else:
				if is_gravity_on == false:
					velocity.y = 20
					is_gravity_on = true	
	else:
		if is_gravity_on == false:
			is_gravity_on = true			

func handle_horizontal_input():
	var input = Vector2.ZERO
	input.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if input.x == 0 or is_dash_on:
		apply_friction()
	else:
		apply_acceleration(input.x)
		
func apply_gravity():
	if is_gravity_on and velocity.y < MOVEMENT_DATA.MAX_FELL_SPEED:
		velocity.y += MOVEMENT_DATA.GRAVITY
		if fast_fell and velocity.y < MOVEMENT_DATA.MAX_FELL_SPEED:
			velocity.y += MOVEMENT_DATA.FAST_FELL_ADDITIONAL_GRAVITY
	if is_on_floor():
		fast_fell = false


func handle_jump():
	if Input.is_action_just_pressed("ui_accept"):
		if additional_jump_state > 0:
			velocity.y = -MOVEMENT_DATA.JUMP_FORCE
			fast_fell = true
			if !is_on_floor() and additional_jump_state > 0 :
				additional_jump_state -= 1
				print(additional_jump_state)

func handle_dash():
	if Input.is_action_just_pressed("ui_lb"):
		print("lb button pressed")
		if !is_on_floor() and additional_jump_state > 0 :
			print("Dash activated")
			is_dash_on = true
			is_gravity_on = false
			additional_jump_state -=1
			velocity.y = 0
			velocity.x = MOVEMENT_DATA.DASH_VELOCITY*current_direction

func reset_dash_state():
	if is_dash_on and velocity.x == 0:
		is_gravity_on = true
		is_dash_on = false
		
func reset_additional_jump_limit():
	if is_on_floor():
		additional_jump_state = MOVEMENT_DATA.ADDITIONAL_JUMP_LIMIT

func apply_friction():
	velocity.x = move_toward(velocity.x, 0, MOVEMENT_DATA.FRICTION)

func apply_acceleration(amount):
	velocity.x = move_toward(velocity.x, MOVEMENT_DATA.HORIZONTAL_TOP_SPEED*sign(amount), MOVEMENT_DATA.HORIZONTAL_ACCELERATION)

func handle_direction():
	if velocity.x == 0:
		pass
	if velocity.x > 0 :
		current_direction = 1
	if velocity.x < 0 :
		current_direction = -1
	handle_raycast_vectors()

func handle_raycast_vectors():
	if current_direction == 1 :
		$LadderChack.cast_to=Vector2(11,0)
	else:
		$LadderChack.cast_to=Vector2(-11,0)

func handle_sprite_rotation():
	if current_direction == 1 :
		get_node("AnimatedSprite").flip_h = true
	else:
		get_node("AnimatedSprite").flip_h = false

func handle_animation_state(is_just_landed: bool):
	handle_sprite_rotation()
	if velocity.x == 0 and is_on_floor():
		$AnimatedSprite.animation = "Idle"
	if velocity.x != 0 and is_on_floor():
		$AnimatedSprite.animation = "Walk"
		if is_just_landed:
			$AnimatedSprite.frame = 1
	if !is_on_floor() and velocity.y > 20:
		$AnimatedSprite.animation = "Glide"
	if !is_on_floor() and velocity.y < 0:
		$AnimatedSprite.animation = "Idle"
	if is_dash_on and !is_gravity_on:
		$AnimatedSprite.animation = "Glide"

func reset_position(reset_position: Vector2):
	transform.origin = reset_position

func handle_fall():
	if(position.y > 300):
		reset_position(sample_reset_position)
