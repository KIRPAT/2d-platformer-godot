extends PlayerState


func apply_friction():
	player.velocity.x = move_toward(player.velocity.x, 0, player.FRICTION)
# Dash activates only on Air

func enter(_msg:= {}) -> void:
	print("Dash")
	player.animation_player.play("Walk")
	player.velocity = Vector2.ZERO
	apply_dash()
	
	
func physics_update(delta: float) -> void:
	apply_friction()
	handle_dash_end()
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)


func apply_dash() -> void:
	var direction_int = 1 if player.direction == player.directions.RIGHT else -1
	player.velocity.x = player.DASH_VELOCITY*direction_int
	
func handle_dash_end():
	if player.velocity.x == 0:
		if not player.is_on_floor():
			state_machine.transition_to("Air")
		else: 
			state_machine.transition_to("Idle")


