extends Resource
class_name PlayerMovementData

export (int) var ADDITIONAL_JUMP_LIMIT = 1
export (int) var DASH_VELOCITY = 500
export (int) var JUMP_FORCE = 150
export (int) var MAX_SPEED = 50
export (int) var MAX_FELL_SPEED = 300
export (int) var FRICTION = 30
export (int) var GRAVITY = 5
export (int) var FAST_FELL_ADDITIONAL_GRAVITY = 3
export (int) var HORIZONTAL_TOP_SPEED = 100
export (int) var HORIZONTAL_ACCELERATION = 10
export (int) var LADDER_SPEED = 100
