class_name PlayerV2
extends KinematicBody2D

export (int) var ADDITIONAL_JUMP_LIMIT = 1
export (int) var DASH_VELOCITY = 500
export (int) var JUMP_FORCE = 150
export (int) var MAX_SPEED = 50
export (int) var MAX_FELL_SPEED = 300
export (int) var FRICTION = 30
export (int) var GRAVITY = 5
export (int) var FAST_FELL_ADDITIONAL_GRAVITY = 3
export (int) var HORIZONTAL_TOP_SPEED = 100
export (int) var HORIZONTAL_ACCELERATION = 10
export (int) var LADDER_SPEED = 100

var velocity: Vector2 = Vector2.ZERO

var sample_reset_position: Vector2 = Vector2(54,36)

onready var animation_player: AnimationPlayer = $AnimationPlayer
onready var player_manager_autoload = get_node("/root/PlayerManagerAutoload") 

enum directions {RIGHT, LEFT}
var direction = directions.RIGHT
 
var is_gravity_on = true
var fast_fell = false
var additional_jump_state: int
var is_dash_on = false
var is_powerup_on = false
var is_alive = true

func _ready():
	player_manager_autoload.connect("health_deplated", self, "_handle_health_depleted")
	var is_alive = true
	
func _init():
	z_index = 10
	
func _physics_process(delta):
	$AnimatedSprite.flip_h = true if direction == directions.RIGHT else false
	$Sprite.flip_h = true if direction == directions.LEFT else false
	$LadderChack.cast_to = Vector2(11,0) if direction == directions.RIGHT else Vector2(-11,0)

func _handle_health_depleted():
	print("Health depleted, player dies. .")
	killPlayer()
	#self.transform.origin = sample_reset_position
	
func killPlayer():
	if is_alive:
		is_alive = false
		$StateMachine.transition_to("Death")
