extends PlayerState

# Utility Functions #
func apply_friction():
	player.velocity.x = move_toward(player.velocity.x, 0, player.FRICTION)

func apply_acceleration(amount):
	player.velocity.x = move_toward(player.velocity.x, player.HORIZONTAL_TOP_SPEED*sign(amount), player.HORIZONTAL_ACCELERATION)

func enter(msg: ={}) -> void:
	player.animation_player.play("Walk")
	print("Walk")

# PlayerStateFunctions #
func physics_update(delta: float) -> void:
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	handle_if_character_should_fall()
	handle_if_character_is_stopping()
	handle_jump_input()
	handle_horizontal_input()
	handle_direction_change()
	

# Handlers and Checks #
func handle_direction_change() -> void:
	var input: float = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if player.direction == player.directions.RIGHT and input < 0:
		player.direction = player.directions.LEFT
	elif player.direction == player.directions.LEFT and input > 0:
		player.direction = player.directions.RIGHT
		

func handle_if_character_should_fall() -> void:
	if not player.is_on_floor() and !player.is_on_wall():
		print ("I'm not on the floor")
		state_machine.transition_to("Air")
				
func handle_horizontal_input() -> void:
	var input: float = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if input == 0:
		apply_friction()
	else:
		apply_acceleration(input)
	

func handle_jump_input():
	if Input.is_action_just_pressed("ui_accept"):
		state_machine.transition_to("Air", {do_jump = true})

func handle_if_character_is_stopping():
	var input: float = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if is_equal_approx(player.velocity.x, 0.0) and input == 0:
		state_machine.transition_to("Idle")
