extends KinematicBody2D

export (Vector2) var velocity = Vector2.ZERO
export (Vector2) var direction = Vector2.RIGHT
export (int) var speed = 0


func _physics_process(delta):
	handle_movement()
	
	move_and_slide(velocity, Vector2.UP)
	handle_direction()
	
func handle_movement():
	velocity = direction*speed
	
func handle_direction():
	var found_wall = is_on_wall()
	var found_ledge = not $LedgeCheckRayCast.is_colliding()
	
	if found_wall or found_ledge: 
		direction *= -1
		
	if direction == Vector2.RIGHT:
		$AnimatedSprite.flip_h = true
		$LedgeCheckRayCast.transform.origin = Vector2(5,-1)
	else:
		$AnimatedSprite.flip_h = false
		$LedgeCheckRayCast.transform.origin = Vector2(-6,-1)
